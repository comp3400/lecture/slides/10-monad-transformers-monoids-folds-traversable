## More Data Types { data-transition="fade none" }

###### Here is a data type

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Writer w a = Writer w a
</code></pre>

<hr>

<pre style="background-color: black; color: white">
ghci> :kind Writer
Writer :: * -> * -> *

ghci> :kind forall w. Writer w
forall w. Writer w :: * -> *
</pre>

---

###### `(Writer w)` is a `Functor`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Writer w a = Writer w a

instance Functor (Writer w) where
  fmap f (Writer w a) =
    Writer w (f a)
</code></pre>

---

###### But is `(Writer w)` an `Applicative`?

* If only we could *"join two `w` together"*
* to make a new `w`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Writer w a = Writer w a

instance Applicative (Writer w) where
  (&lt;*>) :: Writer w (a -> b) -> Writer w a -> Writer w b
  Writer w1 f &lt;*> Writer w2 a =
    Writer
      (<mark>?</mark> w1 w2)
      (f a)
</code></pre>

---

###### But is `(Writer w)` an `Applicative`?

* `Semigroup` can do that!

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Writer w a = Writer w a

instance <mark>Semigroup</mark> w => Applicative (Writer w) where
  (&lt;*>) :: Writer w (a -> b) -> Writer w a -> Writer w b
  Writer w1 f &lt;*> Writer w2 a =
    Writer
      (w1 <mark>&lt;></mark> w2)
      (f a)
</code></pre>

---

###### But is `(Writer w)` an `Applicative`?

* What about `pure`?
* If only we could make a `w` that preserves laws

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Writer w a = Writer w a

instance Semigroup w => Applicative (Writer w) where
  (&lt;*>) :: Writer w (a -> b) -> Writer w a -> Writer w b
  Writer w1 f &lt;*> Writer w2 a =
    Writer
      (w1 &lt;> w2)
      (f a)
  pure a = Writer <mark>?</mark> a
</code></pre>

---

###### But is `(Writer w)` an `Applicative`?

* `Monoid` can do that!

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Writer w a = Writer w a

instance <mark>Monoid</mark> w => Applicative (Writer w) where
  (&lt;*>) :: Writer w (a -> b) -> Writer w a -> Writer w b
  Writer w1 f &lt;*> Writer w2 a =
    Writer
      (w1 &lt;> w2)
      (f a)
  pure a = Writer <mark>identity</mark> a
</code></pre>

---

###### `(Writer w)` is a `Monad`

* By the same constraint
* `Monoid w => Monad (Writer w)`
* We have a monad

<pre><code class="language-haskell hljs" data-trim data-noescape>
instance Monoid w => Monad (Writer w) where
  return = pure
  Writer w1 a >>= f =
    let Writer w2 b = f a
    in  Writer (w1 &lt;> w2) b
</code></pre>

---

###### The `(Writer w)` intuition

* `Writer w` can be thought of as
* *"computing a value `(a)` while joining values of type `(w)`"*
  * parse a string, produce a value and **keep the line number count**
  * perform some I/O, **joining log messages** along the way
* This is performed by the `Monad` *(and superclass)* instance
* It is, after all, just a pair of values

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Writer w a = Writer w a
</code></pre>

---

<div style="font-size:xx-large">
But does <code>Writer</code> have a monad transformer?
</div>

---

## The `Writer` monad transformer

###### Of course!

<pre><code class="language-haskell hljs" data-trim data-noescape>
data WriterT w k a = WriterT (k (Writer w a))

-- PRACTICE
instance Functor (WriterT w k) where -- todo
instance (Applicative k, Monoid w) =>
  Applicative (WriterT w k) where -- todo
instance (Monad k, Monoid w) =>
  Monad (WriterT w k) where -- todo
</code></pre>

---

## `WriterT`

* `(WriterT w k a)` computes a value `(a)` while joining values of type `(w)`
* But also combined with the behaviour of *any other arbitrary monad `(k)`*

<pre><code class="language-haskell hljs" data-trim data-noescape>
data WriterT w k a = WriterT (k (Writer w a))
</code></pre>
