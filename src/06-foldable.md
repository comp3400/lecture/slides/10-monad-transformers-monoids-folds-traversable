## Foldable { data-transition="fade none" }

###### `foldr`

* We have seen right fold before
* `foldr f z list` will
  * replace `Cons` with `f`
  * replace `Nil` with `z`
  * in `list`

<pre><code class="language-haskell hljs" data-trim data-noescape>
foldr :: (a -> b -> b) -> b -> List a -> b
</code></pre>

---

## Foldable

###### `foldr`

<div style="font-size:xx-large">
  The `Foldable` type class generalises the `List` part
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
foldr :: (a -> b -> b) -> b -> <mark>List</mark> a -> b

class Foldable k where
  foldr :: (a -> b -> b) -> b -> <mark>k</mark> a -> b
</code></pre>

---

## Foldable

* There are several instances of `Foldable`
* It has no superclass(es)
* It is otherwise relatively uninteresting

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Foldable k where
  foldr :: (a -> b -> b) -> b -> k a -> b

instance Foldable [] where
instance Foldable Maybe where
instance Foldable ((,) a) where
</code></pre>

---

## Foldable

<div style="font-size:xx-large">
  However &hellip;
</div>

---

## Foldable

###### `Foldable` is closed under composition

<pre style="background-color: black; color: white">
> :type \f -> foldr (\q b -> foldr f b q)
(Foldable f, Foldable g) =>
(a -> b -> b) -> b -> f (g a) -> b
</pre>

<hr>

<pre><code class="language-haskell hljs" data-trim data-noescape>
(Foldable f, Foldable g) =>
(a -> b -> b) -> b -> <mark>f (g</mark> a) -> b
</code></pre>

