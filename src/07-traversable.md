## Traversable { data-transition="fade none" }

###### `sequence`

<pre><code class="language-haskell hljs" data-trim data-noescape>
class (Functor t, Foldable t) => Traversable t where
  sequence :: Applicative k => t (k a) -> k (t a)
</code></pre>

<hr>

<div style="font-size:large">
  *You might see `sequence` named `sequenceA` and this is due to Monad/Applicative history*
</div>

---

## Traversable

###### `traverse`

* You might see `Traversable` defined with `traverse`

<pre><code class="language-haskell hljs" data-trim data-noescape>
class (Functor t, Foldable t) => Traversable t where
  traverse :: Applicative k => (a -> k b) -> t a -> k (t b)
</code></pre>

---

## Traversable

###### `sequence` and `traverse`

* However, `sequence` and `traverse` are *equal in their ability*
* One can be defined in terms of the other

<pre><code class="language-haskell hljs" data-trim data-noescape>
sequence ::
  (Traversable t, Applicative k) =>
  t (k a) -> k (t a)
sequence = traverse id

traverse ::
  (Traversable t, Applicative k) =>
  (a -> k b) -> t a -> k (t b)
traverse f = sequence . fmap f
</code></pre>

---

## Traversable

###### `sequence`

* We will focus on `sequence`
* Knowing that we can always get to `traverse` if necessary

<pre><code class="language-haskell hljs" data-trim data-noescape>
class (Functor t, Foldable t) => Traversable t where
  sequence :: Applicative k => t (k a) -> k (t a)
</code></pre>

---

## Traversable

###### `sequence`

* These functions are *very general*
* With a type made up almost entirely of type variables
* However, pay particular attention to its shape: `t (k a) -> k (t a)`
  * where `(t)` is traversable
  * where `(k)` is any applicative

<pre><code class="language-haskell hljs" data-trim data-noescape>
class (Functor t, Foldable t) => Traversable t where
  sequence :: Applicative k => t (k a) -> k (t a)
</code></pre>

---

## Reminder!

###### Why do we even do these things?

* We abstract away disparate operations to a common *interface*
* Precisely and only to **reduce code repetition**
* We can in this case &hellip;
* &hellip; *because we are Functional Programming*

---

## Reminder!

###### Why do we even do these things?

* That is to say, we are following a very common software engineering principle
* A principle that exists independently of Functional Programming
* However, we can take this principle further than otherwise, because of our commitment to FP
* Without this commitment, these generalisations would fall apart

---

## `sequence`

<div style="font-size:xx-large">
  Let's look at the type of problems that `sequence` solves
</div>

---

## Traversable

###### `sequence`

* When might we want to use this function?
* Note, that it works on **all applicative functors `(k)`**
* and **all traversables `(t)`**

<pre style="background-color: black; color: white; font-size: 48px">
List (k a) -> k (List a)
</pre>

---

## Traversable

<div style="font-size:xx-large">
  For simplicity, let's motivate `sequence`, specialised to traversing `List`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
sequence :: Applicative k => List (k a) -> k (List a)
</code></pre>

---

## Traversable

###### motivating `sequence`

<div style="font-size:xx-large">
  Consider this pseudo-code
</div>

<pre><code class="language-java" data-trim data-noescape>
twizzle(list) {
  var r = List.empty
  for(var i = 0; i &lt; list.length; i++) {
    if(list[i] == null) 
      return null
    else 
      r.add(list[i])
  }
  return r
}
</code></pre>

---

## Traversable

###### motivating `sequence`

* It might be said that the function `twizzle` has an argument of the shape:
  * *"list of possibly null values"*
  * Haskell notation: `List (PossiblyNull a)`

<pre><code class="language-java" data-trim data-noescape>
twizzle(<mark>list</mark>) {
  var r = List.empty
  for(var i = 0; i &lt; list.length; i++) {
    if(list[i] <mark>== null</mark>) 
      return null
    else 
      r.add(list[i])
  }
  return r
}
</code></pre>

<hr>

<div style="font-size:large">
  Reminder: `sequence :: Applicative k => List (k a) -> k (List a)`
</div>

---

## Traversable

###### motivating `sequence`

* It might also be said that the function `twizzle` returns a value of the shape:
  * *"possibly null, list of values"*
  * Haskell notation: `PossiblyNull (List a)`

<pre><code class="language-java" data-trim data-noescape>
twizzle(list) {
  var r = List.empty
  for(var i = 0; i &lt; list.length; i++) {
    if(list[i] == null) 
      return <mark>null</mark>
    else 
      r.add(list[i])
  }
  return <mark>r</mark>
}
</code></pre>

<hr>

<div style="font-size:large">
  Reminder: `sequence :: Applicative k => List (k a) -> k (List a)`
</div>

---

## Traversable

###### motivating `sequence`

* The function `twizzle` has the structure:
  * `List (PossiblyNull a) -> PossiblyNull (List a)`
* The function is implemented as:
  * *if any list element is null, return null*
  * *otherwise, return the list of values*

<pre><code class="language-java" data-trim data-noescape>
twizzle(list) {
  var r = List.empty
  for(var i = 0; i &lt; list.length; i++) {
    if(list[i] == null) 
      return null
    else 
      r.add(list[i])
  }
  return r
}
</code></pre>

<hr>

<div style="font-size:large">
  Reminder: `sequence :: Applicative k => List (k a) -> k (List a)`
</div>

---

## Traversable

###### motivating `sequence`

* We have seen `PossiblyNull` before:
* And its `Applicative` instance does *exactly this*
  * As soon as the *empty* or *null* value is seen, stop/empty

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Optional a = Empty | Full a
data Maybe a = Nothing | Just a -- aka

instance Applicative Optional where
  pure = Full
  Full f &lt;*> Full a = Full (f a)
  <mark>Empty</mark> &lt;*> _ = Empty -- &lt;-- stop
  _ &lt;*> <mark>Empty</mark> = Empty -- &lt;-- stop
</code></pre>

<hr>

<div style="font-size:large">
  Reminder: `sequence :: Applicative k => List (k a) -> k (List a)`
</div>

---

## Traversable

###### motivating `sequence`

* Typical problems that are solved in the way `twizzle` is implemented &hellip;
* are **already solved** by `sequence`
* In fact, here is the code: `twizzle = sequence`

<pre><code class="language-java" data-trim data-noescape>
// List (Optional a) -> List (Optional a)
twizzle(list) {
  var r = List.empty
  for(var i = 0; i &lt; list.length; i++) {
    if(list[i] == null) 
      return null
    else 
      r.add(list[i])
  }
  return r
}
</code></pre>

<hr>

<div style="font-size:large">
  Reminder: `sequence :: Applicative k => List (k a) -> k (List a)`
</div>

---

## Traversable

###### motivating `sequence`

<div style="font-size:xx-large">
  Let's try another example
</div>

---

## Traversable

###### motivating `sequence`

<div style="font-size:xx-large">
  Consider this pseudo-code
</div>

<pre><code class="language-java" data-trim data-noescape>
twozzle(list, x) {
  var r = List.empty
  for(var i = 0; i &lt; list.length; i++) {
    r.add(list[i].apply(x))
  }
  return r
}
</code></pre>

---

## Traversable

###### motivating `sequence`

* The first argument to function `twozzle` is a list of *"things that have an `apply` function"*

<pre><code class="language-java" data-trim data-noescape>
twozzle(<mark>list</mark>, x) {
  var r = List.empty
  for(var i = 0; i &lt; list.length; i++) {
    r.add(list[i].<mark>apply</mark>(x))
  }
  return r
}
</code></pre>

<hr>

<div style="font-size:large">
  Reminder: `sequence :: Applicative k => List (k a) -> k (List a)`
</div>

---

## Traversable

###### motivating `sequence`

* The second argument to function `twozzle` is a value passed to all the `apply` functions*

<pre><code class="language-java" data-trim data-noescape>
twozzle(list, <mark>x</mark>) {
  var r = List.empty
  for(var i = 0; i &lt; list.length; i++) {
    r.add(list[i].apply(<mark>x</mark>))
  }
  return r
}
</code></pre>

<hr>

<div style="font-size:large">
  Reminder: `sequence :: Applicative k => List (k a) -> k (List a)`
</div>

---

## Traversable

###### motivating `sequence`

* The return value of function `twozzle` is a list of values returned by the `apply` functions

<pre><code class="language-java" data-trim data-noescape>
twozzle(list, x) {
  var r = List.empty
  for(var i = 0; i &lt; list.length; i++) {
    <mark>r.add</mark>(list[i].apply(x))
  }
  return <mark>r</mark>
}
</code></pre>

<hr>

<div style="font-size:large">
  Reminder: `sequence :: Applicative k => List (k a) -> k (List a)`
</div>

---

## Traversable

###### motivating `sequence`

* The first argument to the function `twozzle` has the structure <pre style="font-size: 20px; width:100%"><code>List (x -> a)</code></pre>
* The second argument to the function `twozzle` has the structure <pre style="black; font-size: 20px; width:100%"><code>x</code></pre>
* The return value of the function `twizzle` has the structure <pre style="font-size: 20px; width:100%"><code>List a</code></pre>

<hr>

<div style="font-size:large">
  Reminder: `sequence :: Applicative k => List (k a) -> k (List a)`
</div>

---

## Traversable

###### motivating `sequence`

<div style="font-size: 20px; width:100%">  
  <ul>
    <li>
      <code>
        twozzle ::
          List (<mark>x -></mark> a) -> <mark>x -></mark> List a
      </code>
    </li>
    <li>
      <code>
      sequence :: Applicative k =>
        List (<mark>k</mark> a) -> <mark>k</mark> (List a)
      </code>
    </li>
  </ul>
</div>

<pre><code class="language-java" data-trim data-noescape>
twozzle(list, x) {
  var r = List.empty
  for(var i = 0; i &lt; list.length; i++) {
    r.add(list[i].apply(x))
  }
  return r
}
</code></pre>

---

## Traversable

###### motivating `sequence`

<div style="font-size:xx-large">
  `twozzle = sequence`
</div>

<pre><code class="language-java" data-trim data-noescape>
twozzle(list, x) {
  var r = List.empty
  for(var i = 0; i &lt; list.length; i++) {
    r.add(list[i].apply(x))
  }
  return r
}
</code></pre>

---

## Traversable

###### motivating `sequence`

<div style="font-size:xx-large">
  One more &hellip;
</div>

---

## Traversable

###### motivating `sequence`

<div style="font-size:xx-large">
  Consider this pseudo-code
</div>

<pre><code class="language-java" data-trim data-noescape>
twuzzle(list) {
  var r = List.empty;
  for(var i = 0; i &lt; list.length; i++) {
    var s = List.empty
    for(var j = 0; j &lt; list[i].length; j++) {
      s.add(list[i][j])
    }
    r.append(s)
  }
  return r
}
</code></pre>

---

## Traversable

###### motivating `sequence`

* The argument to function `twuzzle` is a list of lists
* The return value of function `twuzzle` is a list of lists

<pre><code class="language-java" data-trim data-noescape>
twuzzle(<mark>list</mark>) {
  var r = List.empty;
  for(var i = 0; i &lt; list.length; i++) {
    var s = List.empty
    for(var j = 0; j &lt; list[i].length; j++) {
      s.add(list[i][j])
    }
    r.append(s)
  }
  return <mark>r</mark>
}
</code></pre>

<hr>

<div style="font-size:large">
  Reminder: `sequence :: Applicative k => List (k a) -> k (List a)`
</div>

---

## Traversable

###### motivating `sequence`

<div style="font-size:x-large">
  Implemented by, *for every list in the list* &hellip;
</div>

<pre><code class="language-java" data-trim data-noescape>
twuzzle(list) {
  var r = List.empty;
  <mark>for(var i = 0; i &lt; list.length; i++)</mark> {
    var s = List.empty
    for(var j = 0; j &lt; list[i].length; j++) {
      s.add(list[i][j])
    }
    r.append(s)
  }
  return r
}
</code></pre>

<hr>

<div style="font-size:large">
  Reminder: `sequence :: Applicative k => List (k a) -> k (List a)`
</div>

---

## Traversable

###### motivating `sequence`

<div style="font-size:x-large">
  *&hellip; then for every element in each of those lists &hellip;*
</div>

<pre><code class="language-java" data-trim data-noescape>
twuzzle(list) {
  var r = List.empty;
  for(var i = 0; i &lt; list.length; i++) {
    var s = List.empty
    <mark>for(var j = 0; j &lt; list[i].length; j++)</mark> {
      s.add(list[i][j])
    }
    r.append(s)
  }
  return r
}
</code></pre>

<hr>

<div style="font-size:large">
  Reminder: `sequence :: Applicative k => List (k a) -> k (List a)`
</div>

---

## Traversable

###### motivating `sequence`

* *construct a list of lists*
* In other words, take the **cartesian product** of a list of lists

<pre><code class="language-java" data-trim data-noescape>
twuzzle(list) {
  var r = List.empty;
  for(var i = 0; i &lt; list.length; i++) {
    var s = List.empty
    for(var j = 0; j &lt; list[i].length; j++) {
      <mark>s.add(list[i][j])</mark>
    }
    r.append(s)
  }
  return r
}
</code></pre>

<hr>

<div style="font-size:large">
  Reminder: `sequence :: Applicative k => List (k a) -> k (List a)`
</div>

---

## Traversable

###### motivating `sequence`

* That is what the `instance Applicative List` does
* Have a look at its implementation!

<hr>

<div style="font-size:large">
  Reminder: `sequence :: Applicative k => List (k a) -> k (List a)`
</div>

---

## Traversable

###### motivating `sequence`

<div style="font-size: 20px; width:100%">  
  <ul>
    <li>
      <code>
        twuzzle ::
          List (<mark>List</mark> a) -> <mark>List</mark> (List a)
      </code>
    </li>
    <li>
      <code>
      sequence :: Applicative k =>
        List (<mark>k</mark> a) -> <mark>k</mark> (List a)
      </code>
    </li>
  </ul>
</div>

<pre><code class="language-java" data-trim data-noescape>
twuzzle(list) {
  var r = List.empty;
  for(var i = 0; i &lt; list.length; i++) {
    var s = List.empty
    for(var j = 0; j &lt; list[i].length; j++) {
      <mark>s.add(list[i][j])</mark>
    }
    r.append(s)
  }
  return r
}
</code></pre>

---

## Traversable

###### motivating `sequence`

<div style="font-size:xx-large">
  `twuzzle = sequence`
</div>

<pre><code class="language-java" data-trim data-noescape>
twuzzle(list) {
  var r = List.empty;
  for(var i = 0; i &lt; list.length; i++) {
    var s = List.empty
    for(var j = 0; j &lt; list[i].length; j++) {
      s.add(list[i][j])
    }
    r.append(s)
  }
  return r
}
</code></pre>

---

## Traversable

###### Summary: motivating `sequence`

* At first glance, `twizzle`, `twozzle` and `twuzzle` all appear to solve different problems
* However, they are all the same problem, **generalised to `sequence`**
* We would not repeat, for example, a `List.sort` function for each list element type
* We would instead exploit a software engineering principle of generalisation
* For the same principled reason, we have done this with these three functions
* Note: we have seen `sequence`, specialised to `List`, with only three examples

---

## Traversable

* Note:
  * we have seen the function `sequence`
  * specialised to `List`
  * with three examples of applicative functors

<div style="text-align:center">
  <a href="images/tip-of-iceberg.jpg">
    <img src="images/tip-of-iceberg.jpg" alt="Tip of the Iceberg" style="width:640px"/>
  </a>
</div>

---

## Traversable

###### Practice

* Look at the `traverse` function
* Rewrite `traverse` using `sequence`
* Rewrite `sequence` using `traverse`
* Construct examples using the `traverse` function using your favourite `Applicative` instance
* Is `Traversable` closed under composition?

---

## Semigroups and Monoids

###### Exercises

<div style="font-size:x-large">
  <a href="https://gitlab.com/comp3400/lecture/code/10-monad-transformers-monoids-folds-traversable">https://gitlab.com/comp3400/lecture/code/10-monad-transformers-monoids-folds-traversable</a>
</div>

