## More Abstractions { data-transition="fade none" }

###### Closure

* What do we mean when we say that functors in general are *closed under composition*?
* Take any two arbitrary functors and and compose them
* We always get a **functor** out the other side &mdash; composition is *closed*

<div style="font-size: x-large; text-align: center">
<code>F &#x2218; F &#x2192; F</code>
</div>

---

###### Closure

* We can witness the closed composition of functors in Haskell
* the `Composition` data type is merely a wrapper to assist with type classes

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Composition f g a = Composition (f (g a))

-- this will always be possible
instance (Functor f, Functor g) =>
  Functor (Composition f g) where -- todo
</code></pre>

---

###### Remember the `Monad` laws?

* `(>>=)` then `(>>=)` can occur in either order *(associativity)*
* `(>>=)` with `pure` on the left does nothing *(left identity)*
* `(>>=)` with `pure` on the right does nothing *(right identity)*

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- law of associativity
forall f g h. (f >>= g) >>= h == f >>= (\x -> g x >>= h)
-- law of left identity
forall f x. pure x >>= f == f x
-- law of right identity
forall x. x >>= pure == x
</code></pre>

---

## Abstractions

###### Let's talk about associativity

<div style="font-size: x-large; text-align: center">
We say that an operation <code>&#x2295;</code> is associative, if we do <code>&#x2295;</code> twice and we get the same result, regardless of the order in which we perform it

<code>(x &#x2295; y) &#x2295; z == x &#x2295; (y &#x2295; z)</code>
</div>

---

## Abstractions

###### Let's talk about associativity

* What are some simple operations that satisfy associativity?
* On what type of arguments does it operate?
* Are the arguments that go in of the same type as what comes out?
* Is the operation *closed under associativity*?

---

## Abstractions

###### Associativity

* **Addition** on the set of **integers** is a closed, associative operation
* **Multiplication** on the set of **integers** is a closed, associative operation

<div style="font-size: x-large; text-align: center">
<code>x, y, z &#x2208; &#x2124;. (x + y) + z == x + (y + z)</code>

<code>x, y, z &#x2208; &#x2124;. (x * y) * z == x * (y * z)</code>
</div>

---

## Abstractions

###### Associativity &mdash; here are some more!

<div style="font-size: x-large; text-align: center">
**(++)** on the set of **lists** is a closed, associative operation

<code>x, y, z &#x2208; List a. (x ++ y) ++ z == x ++ (y ++ z)</code>
</div>

---

## Abstractions

###### Associativity &mdash; here are some more!

<div style="font-size: x-large; text-align: center">
**choice** on the set of **optionals** is a closed, associative operation

<code>x, y, z &#x2208; Optional a. (x &lt;|> y) &lt;|> z == x &lt;|> (y &lt;|> z)</code>
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
Full x &lt;|> _ = Full x
Empty &lt;|> y = y
</code></pre>

---

## Abstractions

###### Associativity &mdash; here are some more!

<div style="font-size: x-large; text-align: center">
**function composition** on the set of **endomorphisms** is a closed, associative operation

<code>x, y, z &#x2208; a -> a. (x . y) . z == x . (y . z)</code>
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
f . g = \x -> f (g x)
</code></pre>

<hr>

<div style="font-size: large; text-align: center">
<ul>
  <li>
    <i>endo</i>: Greek &#x1F14;&#957;&#x3B4;&#x3BF;&#x3BD; (endon) meaning <b>within, inner, absorbing, or containing</b>
  </li>
  <li>
    <i>morph</i>: Greek μορφή (morph&#x1E17;) meaning <b>form, shape</b>
  </li>
  <li>
    <i>endomorphism</i>: a transformation from a set to itself
  </li>
  <li>
    <i>Haskell</i>: <code>a -> a</code>
  </li>
</ul>
</div>

---

## Semigroup

###### Nomenclature

<div style="font-size: x-large; text-align: center">
The common name for a closed, associative operation on a set is <b>Semigroup</b>
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- law: (x &lt;> y) &lt;> z == x &lt;> (y &lt;> z)
class Semigroup a where
  (&lt;>) :: a -> a -> a
</code></pre>

---

## Semigroup

###### We might instance this type class

<pre><code class="language-haskell hljs" data-trim data-noescape>
instance Semigroup Int where ...
instance Semigroup (Optional a) where ...
instance Semigroup (List a) where ...
instance Semigroup (a -> a) where ...
</code></pre>

---

## Laws

###### We have seen two other laws

<div style="font-size: x-large; text-align: center">
for the closed, associative operation <code>&#x2295;</code> and the identity <code>&#248;</code>

<ol>
  <li>
    right identity: <code>x &#x2295; &#248; == x</code>
  </li>
  <li>
    left identity: <code>&#248; &#x2295; x == x</code>
  </li>
</ol>
</div>

---

## Laws

###### We have seen two other laws

<div style="font-size: x-large; text-align: center">
All the semigroups we have seen so far have left and right identities

<ul>
  <li>
    addition on integers: <b><code>0</code></b>
  </li>
  <li>
    multiplication on integers: <b><code>1</code></b>
  </li>
  <li>
    (&lt;|>) on optionals: <b><code>Empty</code></b>
  </li>
  <li>
    (++) on lists: <b><code>Nil</code></b>
  </li>
  <li>
    (.) on endomorphisms: <b><code>\\x -> x</code></b> <i>(or <b><code>id</code></b>)</i>
  </li>
</ul>
</div>

---

## Monoid

###### Nomenclature

<div style="font-size: x-large; text-align: center">
A semigroup that has a value satisfying left and right identity is called a <b>Monoid</b>
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- law: x &lt;> identity == x
class Semigroup a => Monoid a where
  identity :: a
</code></pre>

---

## Monoid

###### We might also instance this type class

<pre><code class="language-haskell hljs" data-trim data-noescape>
instance Monoid Int where
  identity = 0
instance Monoid (Optional a) where
  identity = Empty
instance Monoid (List a) where where
  identity = Nil
instance Monoid (a -> a) where
  identity = id
</code></pre>

---

## Monoid

###### Superclass/subclass

* `Monoid` and `Semigroup` are defined on types with kind <code>*</code>
* That is, they are *not* higher-kinded like `Monad`, etc
* `Monoid` is a subclass of `Semigroup`
* *all things that are monoids are also semigroups*
* *not all things that are semigroups are necessarily monoids*
* Can you think of any?

---

## Semigroup/Monoid

###### Superclass/subclass

<pre><code class="language-haskell hljs" data-trim data-noescape>
data NonEmptyList a = NonEmptyList a [a]

-- closed: appending two non-empty lists
--         makes a non-empty list
-- associative: appending in any order gives the same result
instance Semigroup (NonEmptyList a) where
  h1 t1 &lt;> h2 t2 =
    NonEmptyList h1 (t1 ++ h2 : t2)

instance Monoid (NonEmptyList a) where
  identity =
    <mark>?</mark>
</code></pre>

<div style="font-size: x-large; text-align: center">
but what is the identity?
</div>

---

## Semigroup/Monoid

###### A point on history

* We have been calling the `Monoid` identity, as it is
* The `identity` function we have defined, is called `mempty` in the base library

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- Haskell base library definition
class Semigroup a => Monoid a where
  mempty :: a
</code></pre>

---

## Semigroup/Monoid

###### Practice <a href="https://comp3400.gitlab.io/lecture/code">https://comp3400.gitlab.io/lecture/code</a>

<div style="display: flex; flex: 1; font-size: 16px">
<div class="col">
<pre><code class="language-haskell hljs" data-trim data-noescape>
import Prelude hiding (Semigroup((&lt;>)), Monoid)

data List a = Nil | Cons a (List a)
  deriving (Eq, Show)

instance Functor List where
  fmap _ Nil = Nil
  fmap f (Cons h t) = Cons (f h) (fmap f t)

data Optional a = Empty | Full a
  deriving (Eq, Show)

class Semigroup a where
  (&lt;>) :: a -> a -> a

class Semigroup a => Monoid a where
  identity :: a

instance Semigroup Int where
  -- addition
  (&lt;>) = error "todo: instance Semigroup Int"

instance Monoid Int where
  identity = 0

instance Semigroup (List a) where
  (&lt;>) = error "todo: instance Semigroup (List a)"

instance Monoid (List a) where
  identity = error "todo: instance Monoid (List a)"

instance Semigroup (Optional a) where
  (&lt;>) = error "todo: instance Semigroup (Optional a)"

instance Monoid (Optional a) where
  identity = error "todo: instance Monoid (Optional a)"
</code></pre>
</div>
<div class="col">
<pre><code class="language-haskell hljs" data-trim data-noescape>
-- need to wrap a function due to Haskell reasons
data Endomorphism a = Endomorphism (a -> a)

runEndomorphism :: Endomorphism a -> a -> a
runEndomorphism (Endomorphism f) = f

instance Semigroup (Endomorphism a) where
  (&lt;>) = error "todo: instance Semigroup (Endomorphism a)"

instance Monoid (Endomorphism a) where
  identity = error "todo: instance Monoid (Endomorphism a)"

data Predicate a = Predicate (a -> Bool)

-- to join two predicates
-- both bool values must be True (&amp;&amp;)
instance Semigroup (Predicate a) where
  (&lt;>) = error "todo: instance Semigroup (Predicate a)"

instance Monoid (Predicate a) where
  identity = error "todo: instance Monoid (Predicate a)"

data Minimum = Minimum Int
  deriving (Eq, Show)

-- find the minimum of two Int values
instance Semigroup Minimum where
  (&lt;>) = error "todo: instance Semigroup Minimum"
</code></pre>
</div>
<div class="col">
<pre><code class="language-haskell hljs" data-trim data-noescape>
instance Monoid Minimum where
  identity = error "todo: instance Monoid Minimum"

-- if a and b are semigroups, then (a, b) is a semigroup
instance (Semigroup a, Semigroup b) => Semigroup (a, b) where
  (&lt;>) = error "todo: instance Semigroup (a, b)"

-- if a and b are monoids, then (a, b) is a monoid
instance (Monoid a, Monoid b) => Monoid (a, b) where
  identity = error "todo: instance Monoid (a, b)"

-- foldRight does constructor replacement
-- foldRight f z will replace in the given list
-- * Cons with f
-- * Nil with z
foldRight :: (a -> b -> b) -> b -> List a -> b
foldRight _ b Nil      = b
foldRight f b (Cons h t) = f h (foldRight f b t)

-- add up the numbers in a list
-- use foldRight
-- utilise instance Monoid Int
addUp :: List Int -> Int
addUp = error "todo: addUp"

-- list of functions
listOfFunctions :: List (Endomorphism Int)
listOfFunctions = Endomorphism &lt;$>
  Cons (+1)
  ((Cons (*2))
  ((Cons (div 3))
  ((Cons (+99))
  (Cons (subtract 7) Nil))))

-- a function that applies the list of functions (listOfFunctions)
-- to the given integer
-- see: runEndomorphism
buildUp :: Int -> Int
buildUp = error "todo: buildUp"

-- add up the integers in the list
-- join all the predicates (&amp;&amp;)
addAnd :: List (Int, Predicate String) -> (Int, Predicate String)
addAnd = error "todo: addAnd"
</code></pre>
</div>
</div>
