## Reader and ReaderT { data-transition="fade none" }

###### Simply, wrapping a function

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Reader x a = Reader (x -> a)
</code></pre>

<hr>

<pre style="background-color: black; color: white">
ghci> :kind Reader
Writer :: * -> * -> *

ghci> :kind forall x. Reader x
forall x. Reader x :: * -> *
</pre>

---

###### `(Reader x)` is a `Functor`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Reader x a = Reader (x -> a)

instance Functor (Reader x) where
  fmap f (Reader g) =
    Reader (f . g)
</code></pre>

---

###### `(Reader x)` is an `Applicative`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Reader x a = Reader (x -> a)

instance Applicative (Reader x) where
  pure = Reader . const
  Reader f &lt;*> Reader a =
    Reader (\v -> f v (a v))
</code></pre>

---

###### `(Reader x)` is a `Monad`

* `(>>=)` is *once-inhabited*
* This means:
  * if it compiles
  * and the implementation is *total*
  * then **it is correct**

<pre><code class="language-haskell hljs" data-trim data-noescape>
instance Monad (Reader x) where
  return = pure
  (>>=) = _ -- try this exercise
</code></pre>

---

###### `ReaderT`

* Here is the reader monad transformer
* Combines the behaviour of reader with any arbitrary monad

<pre><code class="language-haskell hljs" data-trim data-noescape>
data ReaderT x k a = ReaderT (x -> k a)
</code></pre>

---

###### `ReaderT`

<div style="font-size:xx-large">
  Try these exercises
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data ReaderT x k a = ReaderT (x -> k a)

instance Functor k => Functor (ReaderT x k) where
  fmap = _
instance Applicative k => Applicative (ReaderT x k) where
  pure = _
  (&lt;*>) = _
instance Monad k => Monad (ReaderT x k) where
  return = pure
  (>>=) = _
</code></pre>

---

###### `ReaderT`

<div style="font-size:xx-large">
  Try experimenting with the `ReaderT` monad
</div>

---

###### `ReaderT` exercises, Step 1

* define `compose`
* This function "joins" two `ReaderT` values together

<pre><code class="language-haskell hljs" data-trim data-noescape>
compose ::
  Monad k =>
  ReaderT b k c
  -> ReaderT a k b
  -> ReaderT a k c
compose = _
</code></pre>

---

###### `ReaderT` exercises, Step 2

* define a `Digit` data type
* *done for you*

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Digit = D0 | D1 | D2 | D3 |  D4 | D5 | D6 | D7 | D8 | D9
  deriving (Eq, Show)
</code></pre>

---

###### `ReaderT` exercises, Step 3

* implement a function `charDigit`
* converts a `Char` to a `Digit` (maybe)
* `:info Maybe` if you are unfamiliar
* *done for you*

<pre><code class="language-haskell hljs" data-trim data-noescape>
charDigit :: ReaderT Char Maybe Digit
charDigit =
  ReaderT (\c -> case c of
    '0' -> Just D0
    '1' -> Just D1
    '2' -> Just D2
    '3' -> Just D3
    '4' -> Just D4
    '5' -> Just D5
    '6' -> Just D6
    '7' -> Just D7
    '8' -> Just D8
    '9' -> Just D9
    _ -> Nothing
  )
</code></pre>

---

###### `ReaderT` exercises, Step 4

* implement a function `charDigits`
* converts a `[Char]` to a `[Digit]` (maybe)
* `:info Maybe` if you are unfamiliar
* use `charDigit` to implement `charDigits`

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- use charDigit
charDigits :: ReaderT String Maybe [Digit]
charDigits = _
</code></pre>

---

###### `ReaderT` exercises, Step 5

* implement a function `firstEvenDigit`
* you will need: `import Data.List(find, elem)`
* use the `find` and `elem` functions to implement `firstEvenDigit`
* return the first even `Digit` in the list (maybe)

<pre><code class="language-haskell hljs" data-trim data-noescape>
firstEvenDigit :: ReaderT [Digit] Maybe Digit
firstEvenDigit = _
</code></pre>

---

###### `ReaderT` exercises, Step 6

* Given a string
  * any characters are not a digit &#8594; `Nothing`
  * any digits are not even &#8594; `Nothing`
  * returns first even digit &#8594; `Just`
  * use `firstEvenDigit`, `compose` and `charDigits` to implement `firstEven`

<pre><code class="language-haskell hljs" data-trim data-noescape>
firstEven :: ReaderT String Maybe Digit
firstEven = _
</code></pre>


---

###### `ReaderT` exercises, Step 7

<div style="font-size:xx-large">
  Run it!
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
runIt :: String -> Maybe Digit
runIt =
  runReaderT firstEven

-- > runIt "12345"
-- Just D2
-- > runIt "abc"
-- Nothing
-- > runIt "123abc45"
-- Nothing
</code></pre>

