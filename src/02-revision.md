## Revision { data-transition="fade none" }

###### Monad Transformers

* Functors and Applicative Functors are closed under composition
* That is, composing any two of these produces another one
* Monad Transformers are often used because Monads in general are not closed under composition
* A Monad Transformer combines the behaviour of two Monads into one one `Monad` instance
* However, the *inner* Monad is a specific data type
* The *outer* Monad is any data type (type parameter)

---

## Revision { data-transition="fade none" }

###### For example

* The `State` data type has a `Monad` instance
* The `StateT` data type has a `Monad` instance
  * with the behavious of `State`
  * combined with any arbitrary `Monad` `(k)`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data State s a = State (s -> (s, a))
</code></pre>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data StateT s k a = StateT (s -> k (s, a))
</code></pre>

---

## Revision

###### The `StateT` Monad Transformer

* If `k` is a `Monad` then `(StateT s k)` is a `Monad`
* The `Monad` instance composes the behaviour of `State` with `k`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data StateT s k a = StateT (s -> k (s, a))

instance Monad k => Monad (StateT s k) where
  return = pure
  StateT x >>= f =
    StateT (\s ->
      x s >>= \(t, a) ->
      let StateT y = f a  in  y t
    )
</code></pre>

---

## Revision

###### The `StateT` Monad Transformer

* The *inner* monad behaviour can be recovered by composing with a neutral monad
* A neutral monad &mdash; one that does nothing &mdash; is `Identity`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Identity x = Identity x
instance Monad Identity where -- todo

data StateT s k a = StateT (s -> k (s, a))
-- State will behave as if k didn't exist
type State s a = StateT s Identity a
</code></pre>

---

## Revision

###### Other Monad Transformers

* Combines the `Optional` monad with any arbitrary monad `(k)`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data OptionalT k a = OptionalT (k (Optional a))
</code></pre>

---

## Revision

###### Other Monad Transformers

* Combines the `Int` reader monad with any arbitrary monad

<pre><code class="language-haskell hljs" data-trim data-noescape>
data IntReaderT k a = ReaderT (Int -> k a)
</code></pre>

